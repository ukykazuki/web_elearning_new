@extends('admin.layout')

@section('title', 'List Transaction')

@section('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
    <section class="content-header">
        <h1>Transaction</h1>
        <ol class="breadcrumb">
            <li>
                <a href="">
                    <i class="fa fa-credit-card">
                        Detail Transaction
                    </i>
                </a>
            </li>
            <li class="active"> List </li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-body">

                    @include('flash::message')

                    <table class="table">
                        <tr>
                            <td>Username</td>
                            <td>{{$detail->data->username}}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{$detail->data->email}}</td>
                        </tr>
                        <tr>
                            <td>Order Id</td>
                            <td>{{$detail->data->order_id}}</td>
                        </tr>
                        <tr>
                            <td>Order Total</td>
                            <td>{{$detail->data->order_total}}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>{{$detail->data->status}}</td>
                        </tr>
                    </table>

                    </div>
                </div>
            </div>
        </div>
    </section>                
@endsection

@section('js')
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>
    <script>

        $('#flash-overlay-modal').modal();
    </script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).on("click", ".modalku", function () {
             var myBookId = $(this).data('id');
             $(".modal-body #bookId").val( myBookId );
        });
    </script>
    <script type="text/javascript">
        @if(session('detail'))
            $('#modal-id').modal('show');
        @endif
    </script>
@endsection