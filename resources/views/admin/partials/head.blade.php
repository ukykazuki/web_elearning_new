<meta charset="utf-8">
<meta name="mobile-web-app-capable" content="yes">
<meta name="theme-color" content="#4285f4">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href='https://adminlte.io/themes/AdminLTE/dist/css/AdminLTE.min.css'>
<link rel="stylesheet" href='https://adminlte.io/themes/AdminLTE/dist/css/skins/_all-skins.min.css'>
<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css'>
<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css'>
<link rel="icon" href="https://icons.better-idea.org/icon?url=youtube.com&size=80..120..200">