@extends('front.layout')
@section('title', 'E-Learning')

@section('css')
<style>
    .lesson-part:hover{
        text-decoration: none;
    }
    .hitam{
        color: #000;
    }
</style>
@endsection

@section('content')
<div class="container">


<div class="row" style="margin-top: 30px;">
  <div class="col-sm-8">
<div class="card">
  <div class="card-header">
    <h4 class="card-title">{{ $lesson->data->title }}</h4>
<p class="float-left">
@if ($lesson->category->data)

    @foreach ($lesson->category->data as $q => $category)
        <span class="badge badge-pill badge-default">
        <a href="{{ route('front.lesson.by.category', ['lesson' => $category->category->slug]) }}" style="color: #000;">{{$category->category->category}}</a>
        </span>
    @endforeach
@endif
</p>
<p  class="float-right">
    By {{ ucfirst($lesson->data->owner) }} - 
    <span title="{{ $lesson->data->published }}">{{Carbon\Carbon::parse($lesson->data->published)->diffForHumans()}}</span>
</p>
  </div>
  <div class="card-body">
    <img class="img-fluid" src="{{ $lesson->data->thumbnail }}" alt="default.jpg" style="max-width: 100%;">
  <hr>
    <p class="text-justify flex-wrap">
     {!! $lesson->data->summary !!}
    </p>
    <hr>
    <h3>Lesson Part</h3>

    @isset ($part->data)
        @php
            $i = 1;
        @endphp
            @foreach ($part->data as $val)
                <ul class="list-group">
                    <a href="{{ url()->current() ."/". $val->slug  }}" class="lesson-part">
                        <li class="list-group-item hitam">
                            {{ $i++ }} &nbsp;&nbsp;&nbsp;
                            <i class="fa fa-film"  > </i> 
                            &nbsp;&nbsp;&nbsp;
                            <span class="hitam">{{ $val->title }}</span>
                            &nbsp;&nbsp;&nbsp; 
                            <span class="badge badge-pill badge-primary" style="color: #fff;">Video</span>
                        </li>
                    </a>
            @endforeach
    @endisset

    @isset ($part->error)
        <p>{{ $part->error->message }}</p>
    @endisset

  </div>
</div>
</div>
  <div class="col-sm-4">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">{{ ucfirst($lesson->data->type) }}
        </h4>
        <p class="card-text">
         @isset ($part->data)Jumlah Kursus: {{count($part->data)}} bagian
         @endisset
        <br>
        Diterbitkan: {{$lesson->data->published}}</p>
      </div>
    </div>
  </div>
</div>

</div>
@endsection

@section('js')

@endsection
