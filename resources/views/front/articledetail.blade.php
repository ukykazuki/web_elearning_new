@extends('front.layout')
@section('title', 'E-Learning')

@section('css')
@endsection

@section('content')
<div class="container">

<div class="card" style="margin-top: 30px;">
  <div class="card-header">
    <h4 class="card-title">{{ $article->data->title }}</h4>
<p class="float-left">
    @foreach ($article->categories->data as $q => $category)
        <span class="badge badge-pill badge-default">
        <a href="{{ route('front.article.by.category', ['article' => $category->category->slug]) }}" style="color: #000;">{{$category->category->category}}</a>
        </span>
    @endforeach
</p>
<p  class="float-right">
    By {{ ucfirst($article->data->owner) }} - 
    <span title="{{ $article->data->published }}">{{Carbon\Carbon::parse($article->data->published)->diffForHumans()}}</span>
</p>
  </div>
  <div class="card-body">
    <img class="img-fluid" src="{{ $article->data->thumbnail }}" alt="default.jpg" style="max-width: 100%;">
  <hr>
    <p class="text-justify flex-wrap">
     {!! $article->data->content !!}
    </p>

  </div>
</div>

@endsection

@section('js')

@endsection
