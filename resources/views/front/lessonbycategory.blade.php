@extends('front.layout')
@section('title', 'E-Learning')
@section('css')
    <style media="screen">
        .row{
            margin-bottom: 20px;
        }
        figure img{
            width: 100%;
            height: 100%;
        }
        .glyphicon-folder-open,
        .glyphicon-user,
        .glyphicon-calendar,
        .glyphicon-eye-open,
        .glyphicon-comment{
            padding: 5px;
        }
.carousel-item{
    padding: 20rem
    box-shadow: inset 0 0 20rem rgba(0,0,0,1);
}

    </style>
@endsection

@section('content')
<div class="row" style="margin-top: 30px;">

{{-- <div class="card-columns"> --}}
<div class="card-columns">
@foreach ($lesson->data as $val) 
{{-- <div class="col-md-4" style="margin-bottom: 10px;"> --}}
 <div class="col-sm-6 col-md-4 col-lg-3">
<div class="card" style="width: 20rem;">
  <a href="{{ route('front.lesson.detail', ['slug' => $val->slug]) }}">
    <img class="card-img-top gambar" src="{{$val->thumbnail}}" alt="Card image cap">
  </a>
              <div class="card-block">
              @if ($val->categories !=)
                 @foreach ($val->categories as $q => $category)
                        <span class="badge badge-pill badge-default">
                            <a href="{{ route('front.lesson.by.category', ['article' => $category->category->slug]) }}" style="color: #000;">{{$category->category->category}}</a>
                        </span> 
                @endforeach
              @endif
              </div>
    <div class="card-body">
     <a href="{{ route('front.lesson.detail', ['slug' => $val->slug]) }}"> <h4 class="card-title" style="color: #000;">{{ $val->title }}</h4></a>
      <p class="card-text">{!! substr($val->summary,0, 100) !!}</p>
    </div>

    <div class="card-footer">
      <div class="float-left">
        <small class="text-muted">    By {{ ucfirst($val->owner) }} - 
      <span title="{{$val->published}}">{{Carbon\Carbon::parse($val->published)->diffForHumans()}}</span>
      </small>
      </div>
      <div class="float-right">
       <small class="text-muted"> {{ucfirst($val->type)}}</small>
      </div>
    </div>

  </div>
</div>

@endforeach
</div>
</div>
</div>



<!--  --> 

<!--  -->
<nav aria-label="Page navigation example">
@if (isset($lesson->meta->pagination))
<?php
$page = $lesson->meta->pagination;
?>
<p class="pull-left"><br><b>Total Data : {{$page->total}}</b></p>
<ul class="pagination justify-content-end">
    @if (isset($page->links->previous))
    <li class="page-item"><a class="page-link" href="{{url('/lesson')}}?page=1">First</a></li>
    <li class="page-item">
      <a class="page-link" href="{{url('/lesson')}}?page={{$page->current_page-1}}" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
      </a>
    </li>
    @else
    <li class="page-item"><a class="page-link disabled">First</a></li>
    <li class="page-item">
      <a class="page-link disabled" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
      </a>
    </li>
    @endif

<?php $x = $page->total_pages; ?>

@for ($i =1; $i<=$x; $i++ )
    @if ($page->current_page==$i)
    <li class="page-item active"><a class="page-link" href="">{{$i}}</a></li>
    @else
    <li class="page-item"><a class="page-link" href="{{url('/lesson')}}?page={{$i}}">{{$i}}</a></li>
    @endif
@endfor

    @if (isset($page->links->next))
    <li class="page-item">
      <a class="page-link" href="{{url('/lesson')}}?page={{$page->current_page+1}}" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
      </a>
    </li>
    <li class="page-item"><a href="{{url('/lesson')}}?page={{$page->total_pages}}">Last</a></li>
    @else
     <li class="page-item">
      <a class="page-link disabled" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link disabled">Last</a></li>
    @endif
</ul>                    
@endif
</nav>
<!--  -->

@endsection

@section('js')
<script type="text/javascript">
$('.carousel').carousel()
</script>
@endsection
