@extends('front.layout')
@section('title', 'Lesson')

@section('css')
<link rel="stylesheet" href="{{url('/')}}dist/cropper.min.css">
<link rel="stylesheet" href="{{url('/')}}dist/cropper.min.css">
    <style media="screen">
        .row{
            margin-bottom: 20px;
        }

        article{
            background-color: #E0E0E0;
            padding: 10px;
            margin-bottom: 10px;
            margin-top: 10px;
        }
        figure img{
            width: 100%;
            height: 100%;
        }
        .glyphicon-folder-open,
        .glyphicon-user,
        .glyphicon-calendar,
        .glyphicon-eye-open,
        .glyphicon-comment{
            padding: 5px;
        }
.carousel-item{
    padding: 20rem
    box-shadow: inset 0 0 20rem rgba(0,0,0,1);
}

    </style>
@endsection

@section('content')
@php

// var_dump(Storage::disk('local'));
@endphp
<!--  -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <?php $max = count($slide); ?>
    @for ($i = 0; $i < $max; $i++)
            <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}"></li>
    @endfor

    
  </ol>
  <div class="carousel-inner" role="listbox">
    @foreach ($slide as $key => $val)
        @if ($key == 0)
            <div class="carousel-item active">
        @else
            <div class="carousel-item">
        @endif
          <img class="d-block img-fluid col-12" src="{{url($val->thumbnail)}}" alt="First slide">
          <div class="carousel-caption d-none d-md-block" style="color: #000;">
            <h3>
  <a href="{{ route('front.article.detail', ['slug' => $val->slug]) }}" style="color: #000;">{{$val->title}}</a></h3>
            <p>{!! substr($val->summary,0, 100) !!}</p>
         </div>
         </div>
    @endforeach


  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<div class="row" style="margin-top: 30px;">
{{-- <div class="card-columns"> --}}
<div class="card-columns">
@foreach ($lesson->data as $val) 
{{-- <div class="col-md-4" style="margin-bottom: 10px;"> --}}
 <div class="col-sm-6 col-md-4 col-lg-3">
<div class="card" style="width: 20rem;">
  <a href="{{ route('front.lesson.detail', ['slug' => $val->slug]) }}">
    <img class="card-img-top gambar" src="{{$val->thumbnail}}" alt="Card image cap">
  </a>
              <div class="card-block">
                @if ($val->categories != null)
                 @foreach ($val->categories as $q => $category)
                        <span class="badge badge-pill badge-default">
                            <a href="{{ route('front.lesson.by.category', ['lesson' => $category->category->slug]) }}" style="color: #000;">{{$category->category->category}}</a>
                        </span> 
                @endforeach
                @endif
              </div>
    <div class="card-body">
     <a href="{{ route('front.lesson.detail', ['slug' => $val->slug]) }}"> <h4 class="card-title" style="color: #000;">{{ $val->title }}</h4></a>
      <p class="card-text">{!! substr($val->summary,0, 100) !!}</p>
    </div>

    <div class="card-footer">
      <div class="float-left">
        <small class="text-muted">    By {{ ucfirst($val->owner) }} - 
      <span title="{{$val->published}}">{{Carbon\Carbon::parse($val->published)->diffForHumans()}}</span>
      </small>
      </div>
      <div class="float-right">
       <small class="text-muted"> {{ucfirst($val->type)}}</small>
      </div>
    </div>

  </div>
</div>

@endforeach
</div>
</div>
</div>

<!--start pagination-->

{{-- </div> --}}
@if (isset($lesson->meta->pagination))
<?php
$page = $lesson->meta->pagination;
?>
<div class="container">
<div class="float-left">
  <b>Total Data : {{$page->total}}</b>
</div>
<div class="float-right">
<nav aria-label="Page navigation example">
  <ul class="pagination justify-content-end float-right">
    @if (isset($page->links->previous))
    <li class="page-item"><a class="page-link" href="{{url('/lesson')}}?page=1">First</a></li>
    <li class="page-item">
      <a class="page-link" href="{{url('/lesson')}}?page={{$page->current_page-1}}" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
      </a>
    </li>
    @else
    <li class="page-item"><a class="page-link disabled">First</a></li>
    <li class="page-item">
      <a class="page-link disabled" aria-label="Previous">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only">Previous</span>
      </a>
    </li>
    @endif

<?php $x = $page->total_pages; ?>

@for ($i =1; $i<=$x; $i++ )
    @if ($page->current_page==$i)
    <li class="page-item active"><a class="page-link" href="">{{$i}}</a></li>
    @else
    <li class="page-item"><a class="page-link" href="{{url('/lesson')}}?page={{$i}}">{{$i}}</a></li>
    @endif
@endfor

    @if (isset($page->links->next))
    <li class="page-item">
      <a class="page-link" href="{{url('/lesson')}}?page={{$page->current_page+1}}" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link" href="{{url('/lesson')}}?page={{$page->total_pages}}">Last</a></li>
    @else
     <li class="page-item">
      <a class="page-link disabled" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link disabled">Last</a></li>
    @endif
</ul>          
</nav>
</div>
</div>
@endif
<!--end pagination-->


@endsection

@section('js')
<script src="{{url('/')}}dist/cropper.min.js"></script>
<script>
  $('.gambar').cropper({
  aspectRatio: 9/ 3,
});
</script>
    <script type="text/javascript">
        $('#carousel').carousel()
    </script>
@endsection
